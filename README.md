Lexical Analytics Utilities
===========================

[![Morimekta](https://img.shields.io/static/v1?label=morimekta.net&message=utils-lexer&color=informational)](https://morimekta.net/utils-lexer/)
[![Docs](https://www.javadoc.io/badge/net.morimekta.utils/lexer.svg)](https://www.javadoc.io/doc/net.morimekta.utils/lexer)
[![Pipeline](https://gitlab.com/morimekta/utils-lexer/badges/master/pipeline.svg)](https://gitlab.com/morimekta/utils-lexer/pipelines)
[![Coverage](https://gitlab.com/morimekta/utils-lexer/badges/master/coverage.svg)](https://morimekta.net/utils-lexer/jacoco-ut/)
[![License](https://img.shields.io/static/v1?label=license&message=apache%202.0&color=informational)](https://apache.org/licenses/LICENSE-2.0)

Utility for doing lexical parsing of some input. It is optimized for
static (string) parsing, not partial input stream parsing, but contains
tokenizer supporting either.
See [morimekta.net/utils](https://morimekta.net/utils/) for procedures on releases.

## Getting Started

To add to `maven`:

```xml
<dependency>
    <groupId>net.morimekta.utils</groupId>
    <artifactId>lexer</artifactId>
    <version>${version}</version>
</dependency>
```

To add to `gradle`:

```
implementation 'net.morimekta.utils:lexer:${version}'
```
