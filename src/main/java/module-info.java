/**
 * Simple lexical tokenizer and parser library.
 */
module net.morimekta.lexer {
    exports net.morimekta.lexer;
    requires net.morimekta.strings;
}