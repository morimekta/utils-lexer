package net.morimekta.lexer;

import net.morimekta.lexer.impl.TestToken;
import net.morimekta.lexer.impl.TestTokenType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class TokenTest {
    public static Stream<Arguments> dataLine() {
        return Stream.of(
                arguments("foo 500 bar", 4, 5, "foo 500 bar"),
                arguments("\n" +
                          "foo 500 bar\n" +
                          "more\n" +
                          "not in line", 5, 5, "foo 500 bar"),
                arguments("500 bar", 0, 5, "·   500 bar"),
                arguments("500 bar", 0, 3, "  500 bar"));
    }

    @ParameterizedTest
    @MethodSource("dataLine")
    public void testLine(String content, int off, int linePos, String line) {
        TestToken token = token(content,
                                off,
                                Math.min(3, content.length() - off),
                                TestTokenType.IDENTIFIER,
                                2,
                                linePos);
        assertThat(token.line().toString(), is(line));
    }


    public static Stream<Arguments> dataDecode() {
        return Stream.of(
                arguments("ab\\b\\f\\n\\r\\t\\\'\\\"\\\\\\u0020\\040\\0\\177·\007",
                          "ab\b\f\n\r\t'\"\\  \0\177·\007"),
                arguments("\uD800\uDC97", "\uD800\uDC97"),
                arguments("\\uD800\uDC97", "\uD800\uDC97"),
                arguments(new String(new char[]{'\uD800', '\\', 'u', 'D', 'C', '9', '7'}), "\uD800\uDC97"),
                arguments("\\uD800\\uDC97", "\uD800\uDC97"));
    }

    @ParameterizedTest
    @MethodSource("dataDecode")
    public void testDecode(String str, String decoded) {
        TestToken token = token("\"" + str + "\"", TestTokenType.STRING);
        assertThat(token.decodeString(false), is(decoded));
        assertThat(token.decodeString(true), is(decoded));
    }

    public static Stream<Arguments> dataDecode_StrictFail() {
        return Stream.of(
                arguments("\\01", "�", "Invalid escaped char: '\\01'"),
                arguments("\\1fbf", "�f", "Invalid escaped char: '\\1fb'"),
                arguments("\\12", "�", "Invalid escaped char: '\\12'"),
                arguments("\\lf", "�f", "Invalid escaped char: '\\l'"),

                arguments("\\uD800b", "�b", "Unmatched high surrogate char: '\\ud800'"),
                arguments("\\uD80", "�", "Invalid escaped unicode char: '\\uD80'"),
                arguments("\\u1-23f", "�f", "Invalid escaped unicode char: '\\u1-23'"),
                arguments("\uDC97\\uD800f", "��f", "Unmatched low surrogate char: '\\udc97'"),
                arguments("\uD800", "�", "Unmatched high surrogate char: '\\ud800'"),
                arguments("\\uD800", "�", "Unmatched high surrogate char: '\\ud800'"),
                arguments("\\uDC97\\uD800f", "��f", "Unmatched low surrogate char: '\\udc97'"),
                arguments("\\uD800\\uDC9", "��", "Invalid escaped unicode char: '\\uDC9'"),
                arguments("\\uD800\\177", "�\177", "Unmatched high surrogate char: '\\ud800'"),
                arguments("\\uD800\\u1-23", "��", "Invalid escaped unicode char: '\\u1-23'"),
                arguments("\\ud800\\uD812", "��", "Unmatched high surrogate char: '\\ud800'"));
    }

    @ParameterizedTest
    @MethodSource("dataDecode_StrictFail")
    public void testDecode_StrictFail(String str, String lenient, String strictMessage) {
        TestToken token = token("\"" + str + "\"", TestTokenType.STRING);
        assertThat(token.decodeString(false), equalTo(lenient));
        try {
            token.decodeString(true);
            fail("no exception: " + strictMessage);
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is(strictMessage));
        }
    }

    @Test
    public void testIsSymbol() {
        assertThat(token("?", TestTokenType.SYMBOL).isSymbol('?'), is(true));
        assertThat(token("?", TestTokenType.STRING).isSymbol('?'), is(true));
        assertThat(token("??", TestTokenType.SYMBOL).isSymbol('?'), is(false));
        assertThat(token("??", TestTokenType.STRING).isSymbol('?'), is(false));
    }

    @Test
    public void testObject() {
        TestToken t = token("foo", TestTokenType.STRING);
        assertThat(token("foo", TestTokenType.STRING), is(t));
        assertThat(token("foo", TestTokenType.SYMBOL), is(not(t)));
        assertThat(t, is(t));
        assertThat(t, is(not("foo")));

        assertThat(t.hashCode(), is(token("foo", TestTokenType.STRING).hashCode()));
        assertThat(t.hashCode(), is(not(token("bar", TestTokenType.STRING).hashCode())));
    }

    public TestToken token(String str, TestTokenType type) {
        return token(str, 0, str.length(), type, 1, 1);
    }

    public TestToken token(String str, int start, int len, TestTokenType type, int lineNo, int linePos) {
        return new TestToken(str.toCharArray(), start, len, type, lineNo, linePos);
    }
}
