package net.morimekta.lexer.impl;

import net.morimekta.lexer.TokenizerBase;

import java.io.IOException;
import java.io.Reader;

public class TestTokenizer extends TokenizerBase<TestTokenType, TestToken> {
    public TestTokenizer(Reader in, int bufferSize, boolean preLoadAll) {
        super(in, bufferSize, preLoadAll);
    }

    @Override
    protected TestToken genericToken(char[] buffer, int offset, int len, TestTokenType type, int lineNo, int linePos) {
        return new TestToken(buffer, offset, len, type, lineNo, linePos);
    }

    @Override
    protected TestToken identifierToken(char[] buffer, int offset, int len, int lineNo, int linePos) {
        return new TestToken(buffer, offset, len, TestTokenType.IDENTIFIER, lineNo, linePos);
    }

    @Override
    protected TestToken stringToken(char[] buffer, int offset, int len, int lineNo, int linePos) {
        return new TestToken(buffer, offset, len, TestTokenType.STRING, lineNo, linePos);
    }

    @Override
    protected TestToken numberToken(char[] buffer, int offset, int len, int lineNo, int linePos) {
        return new TestToken(buffer, offset, len, TestTokenType.NUMBER, lineNo, linePos);
    }

    @Override
    protected TestToken symbolToken(char[] buffer, int offset, int len, int lineNo, int linePos) {
        return new TestToken(buffer, offset, len, TestTokenType.SYMBOL, lineNo, linePos);
    }

    @Override
    protected TestToken nextSymbol() throws IOException {
        return super.nextSymbol();
    }

    @Override
    protected boolean startIdentifier() {
        return lastChar == '$' || super.startIdentifier();
    }

    @Override
    protected boolean allowAfterInteger(int last) {
        return last == 'l' || last == 'L' || super.allowAfterInteger(last);
    }

    @Override
    protected boolean allowAfterFloatingPoint(int last) {
        return last == 'f' || last == 'F' || last == 'd' || last == 'D' || super.allowAfterFloatingPoint(last);
    }
}
